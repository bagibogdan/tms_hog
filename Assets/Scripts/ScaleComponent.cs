using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ScaleComponent : EffectComponent
{
    [SerializeField] private float _scaleMultiplier;
    [SerializeField] private float _scaleDuration;

    public override void DoEffect(Action callback)
    {
        transform.DOScale(transform.localScale * _scaleMultiplier, _scaleDuration).OnComplete(() =>
        {
            callback?.Invoke();
        });
    }
}
