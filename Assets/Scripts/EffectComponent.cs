using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class EffectComponent : MonoBehaviour
{
    public abstract void DoEffect(Action callback);
}
