using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShakeScaleComponent : EffectComponent
{
    [SerializeField] private float _shakeDuration;
    [SerializeField] [Range(0, 1)] private float _shakeStrength;
    [SerializeField] [Range(0, 10)] private int _shakeVibrato;
    [SerializeField] [Range(0, 90)] private int _shakeRandomness;
    [SerializeField] private bool _shakeFadeOut;

    public override void DoEffect(Action callback)
    {
        transform.DOShakeScale(_shakeDuration, _shakeStrength, _shakeVibrato, _shakeRandomness, _shakeFadeOut).OnComplete(() =>
        {
            callback?.Invoke();
        });
    }
}
