using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

[RequireComponent(typeof(SpriteRenderer))]
public class GameItem : MonoBehaviour
{
    [SerializeField] private string _name;

    private EffectComponent _effectComponent;
    private SpriteRenderer _spriteRenderer;

    public string Name => _name;
    public Sprite Sprite => _spriteRenderer.sprite;

    public event Action<string> OnFind;

    private void Awake()
    {
        _effectComponent = GetComponent<EffectComponent>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void OnMouseUpAsButton()
    {
        OnFindMethod();
    }

    public void OnFindMethod()
    {
        _effectComponent.DoEffect(() => 
        {
            OnFind?.Invoke(_name);
            OnFind = null;
            gameObject.SetActive(false);
        });
    }
}
